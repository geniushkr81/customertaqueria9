import { useNavigation } from "@react-navigation/native";
import { ImageBackground, Image, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { View, Text } from "../components/Themed";
import { SearchBar,Card, CheckBox } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import List from "../components/ListResults";
import Product from "../components/ProductResults";
import HeaderApp from "../components/HeaderApp";
import { Button } from "react-native-elements/dist/buttons/Button";
import { useState } from "react";
import { color } from "react-native-reanimated";

export default function DeliverTrack() {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <HeaderApp name="Orden tracking"/>
            <ScrollView style={styles.scroll}>
                <View style={styles.row}>
                <Image source={require("../assets/images/maptracking.png")} />
                </View>  
                <View style={styles.liner}></View>  
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#FEB72B"
    },
    scroll:{
        width:"100%",
        backgroundColor:"#fff",
        borderRadius:10,
        padding:10,
        alignSelf:"center"
    },
    row:{
        alignSelf:"center",
        width:"97%",
        padding:10,
        flexDirection:"row"
    },
    text:{
        color:"#f7067d",
        fontWeight:"bold"
    },
    liner:{
        width:"96%",
        alignSelf:"center",
        borderBottomWidth:1,
        borderBottomColor:"#f7067d",
    },
    column:{
        width:"45%",
        flexDirection:"column"
    },
    deliver:{
        marginTop:10,
        marginLeft:10,
        marginBottom:10,
    }
})