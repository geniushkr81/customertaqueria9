import { useNavigation } from "@react-navigation/native";
import { ImageBackground, Image, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { View, Text } from "../components/Themed";
import { SearchBar,Card } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";

export default function OrdersScreen() {
    const navigation = useNavigation();
    return(
        <View style={styles.container}>
             <ScrollView style={styles.scroll}>
             <ImageBackground  source={require('../assets/images/bg-screen.png')}
            style={[styles.content_bg,{width: '100%', height: '100%'}]}>
                <SearchBar round   placeholder=" buscar..." style={{borderWidth:1}}
                        inputStyle={{backgroundColor: 'white', borderRadius:10, padding:7, fontWeight:"bold"}}
                        searchIcon={{color:"white", size:25, }}
                        containerStyle={{marginLeft:20, width:"90%",marginTop:40}} />
              <View style={{backgroundColor:"#ff00ff00" , top: 0, left: 0, right: 0, bottom: 0,marginLeft:15, justifyContent: 'center', alignItems: "flex-start"}}>
     <Text style={{color:"#F7067D",top:10,left:5, fontWeight:"bold"}}>Historia</Text>
   </View> 
   <View style={styles.groupForm} >
        <TextInput style={styles.textInputBorder} placeholder="Alambre"></TextInput>
        <TextInput style={styles.textInputBorder} placeholder="Alambre"></TextInput>
        <TextInput style={styles.textInputBorder} placeholder="Alambre"></TextInput>
    </View>

     <View style={{backgroundColor:"#ff00ff00" , top: 15,marginLeft:15, justifyContent: 'center', alignItems: "flex-start"}}>
     <Text style={{color:"#F7067D", fontWeight:"bold"}}>Sugerencias</Text>
     </View> 
     <View style={styles.productContainer}>
         <View style={styles.productRow}>
         <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
       <Text style={styles.textCat}>Todo</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat02.png")} />
       <Text style={styles.textCat}>Tacos</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat03.png")} />
       <Text style={styles.textCat}>Alambres</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat04.png")} />
       <Text style={styles.textCat}>Tortas</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
       <Text style={styles.textCat}>Todo</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat03.png")} />
       <Text style={styles.textCat}>Alambres</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat04.png")} />
       <Text style={styles.textCat}>Tortas</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
       <Text style={styles.textCat}>Todo</Text>      
       </TouchableOpacity>
     </View>
     
     </View>   
  
</ImageBackground>
</ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: { 
        flex:1,
        alignItems:"center",
        justifyContent:"center",
    },
    scroll:{
      width:"100%"
    },
    textInputBorder:{
        marginLeft:15,
        marginTop:15,
        color:"#06623B",
        borderBottomColor:"pink",
        borderBottomWidth:1,
        width:"90%",
        fontSize:12,
        paddingLeft:10,
    },
    groupForm: {
        width:"90%",
        backgroundColor:"#ff00ff00",
        alignItems:"center"
    }
    ,
    image:{
        marginTop:10,
        marginBottom: 20,
        marginLeft: 15,
        width:400,
        height:100,
    },
    imageContainer:{
        flex:1,
        flexDirection:"row",
        backgroundColor:"#ff00ff00",
        marginTop:10,
        marginLeft:12,
    },
    productContainer:{
        flexDirection:"column",
        backgroundColor:"#ff00ff00",
        marginTop:5,
        width:"100%",
        height:"100%"
    },
    productRow:{
        flex:1,
        flexWrap:"wrap",
        padding:10,
        backgroundColor:"#ff00ff00",
        flexDirection:"row"      
    }
    ,
    imageCat:{
    margin:5,
        width:90,
        height:90
    },
    textCat:{
        fontSize:12,
        marginLeft:10,
        fontWeight:"300",
        color:"#F7067D"
    },
    imageOrder:{
        marginLeft:19,
        marginBottom:10,
            width:95,
            height:95
        },
    imageProd:{
        margin:10,
            width:60,
            height:60,
            borderRadius:10,
        }
    
})