import React, {useEffect, useRef, useState} from 'react';
import { useNavigation } from "@react-navigation/native";
import { Alert, ImageBackground,Image, StyleSheet, Modal, Pressable, TouchableOpacity } from "react-native";
import { View,Text } from "../components/Themed";
import * as Notifications from "expo-notifications";
import Constants from 'expo-constants';
import { Subscription } from '@unimodules/core';
import { create } from 'apisauce';
//import firebase from '@firebase/app';
//import 'firebase/auth';
//import firebase from 'firebase';
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDxc-zlRS7avcUXj8dQl80BsNKCU1Lr5zE",
  authDomain: "taqueria-47786.firebaseapp.com",
  projectId: "taqueria-47786",
  storageBucket: "taqueria-47786.appspot.com",
  messagingSenderId: "312291137372",
  appId: "1:312291137372:web:c7a9a0f3d7df3b32805399",
  measurementId: "G-4P75EXG8C9"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const analytics = getAnalytics(firebase);

import * as GoogleSignIn from 'expo-google-sign-in';
import * as Facebook from 'expo-facebook';

const getPushToken = () => {

  if(!Constants.isDevice) {
    return Promise.reject('Must use physical device for Push Notifications');
  }
  try {
    return Notifications.getPermissionsAsync()
        .then((statusResult) => {
          return statusResult.status !== 'granted'
              ? Notifications.requestPermissionsAsync()
              : statusResult;
        })
        .then((statusResult) => {
          if(statusResult.status !== 'granted'){
            throw 'Failed to get push token for push notification!';
          }
          return Notifications.getExpoPushTokenAsync();
        })
        .then((tokenData) => tokenData.data);
  } catch (error) {
    return Promise.reject("Couldn't check notifications permissions");
  }
};

export default function LoginScreen(){

  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState("");
  const notificationListener = useRef(Subscription);
  const responseListener = useRef(Subscription);
  const [user, setUser] = useState("");
  const [modalVisible, setModalVisible] = useState(false);

// SignIn Facebook
const facebookLogIn = async () => {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync('499013231674177', {
        permissions: ['public_profile'],
      });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`)
          .then(response => response.json())
          .then(data => {
            setLoggedinStatus(true);
            setUserData(data);
          })
          .catch(e => console.log(e))
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

// Somewhere in your code
const signIn = async () => {
  await GoogleSignIn.signIn({
    clientId: '390276598926-22u0vuggd2rad7jl22ehl58hh2o02lq0.apps.googleusercontent.com',
  });
  this._syncUserWithStateAsync();
};

const _syncUserWithStateAsync = async () => {
  const user = await GoogleSignIn.signInSilentlyAsync();
  this.setState({user});
};

const signOutAsync = async () => {
  await GoogleSignIn.signOutAsync();
  this.setState({user: null});
};
const signInAsync = async () => {
    try {
      await GoogleSignIn.askForPlayServicesAsync();
      const { type, user } = await GoogleSignIn.signInAsync();
      if (type === 'success') {
        this._syncUserWithStateAsync();
      }
    } catch ({ message }) {
      alert('login: Error:' + message);
    }
  };

  const onPressBtn = () => {
    if (user) {
      this.signOutAsync();
    } else {
      signInAsync();
    }
  };


    getPushToken().then((pushToken) => {
      setExpoPushToken(pushToken);
      if(pushToken) {
       // Alert.alert('Token Alert', pushToken);
        console.log(pushToken);
    // ApiSauce
        const clientAPI = create ({
          baseURL:'http://18.217.43.19:3000/tokens/'
        });
        clientAPI.post('create',{
          token: pushToken,
          message:'new token',
          status:'active'
        });
    // ApiSauce
      }
    });
  const subscription = Notifications.addNotificationReceivedListener(notification => {
    console.log("Notificacion: " + notification.request.content.body)
    Alert.alert('Push Notification',notification.request.content.body);
  });

    const navigation = useNavigation();
    return(
  
        <View style={styles.container}>
          {/* <Modal
            animationType='slide'
            transparent={true}
            visible={modalVisible}
            onRequestClose={
              () => {
                Alert.alert('Modal has been closed.');
                setModalVisible(!modalVisible);
              }}>
                <View style={styles.centeredView}>
                  <View style={styles.modalView}>
                    <Text>Sugerencia del dia</Text>
                    <Text>AQUI SE MOSTRARA LA IMAGEN DE LA SUGERENCIA DEL DIA</Text>
                    <Pressable onPress={() => setModalVisible(!modalVisible)}><Text>Cerrar</Text></Pressable>
                  </View>
                </View>
              </Modal> */}
            <ImageBackground  source={require('../assets/images/bg-login.png')}
            style={[styles.content_bg,{width: '100%', height: '100%'}]}>
      <Image style={styles.image} source={require("../assets/images/logo.png")} />

<TouchableOpacity style={styles.inactive_btn}
onPress={()=> navigation.navigate('Panel')}>
  <Text style={styles.btnText}>Conectar con Email</Text>
</TouchableOpacity>
<TouchableOpacity style={styles.inactive_btn} onPress={facebookLogIn}>
  <Text style={styles.btnText}>Conectar con Facebook</Text>
</TouchableOpacity>
<TouchableOpacity style={styles.inactive_btn} onPress={onPressBtn}>
  <Text style={styles.btnText}>Conectar con Google</Text>
</TouchableOpacity>
<View
  style={{
    borderBottomColor: '#fff',
    borderBottomWidth: 2,
  }}
/>
<TouchableOpacity style={styles.inactive_btn_driver} onPress={() => navigation.navigate('DeliverLogin')}>
  <Text style={styles.btnTextDriver}>Repartidor</Text>
</TouchableOpacity>
{/* <TouchableOpacity style={styles.inactive_btn} onPress={() => SignUp('admin','admin')}>
  <Text style={styles.btnText}>{expoPushToken}</Text>
</TouchableOpacity> */}
{/* <GoogleSigninButton
   style={{ width: 192, height: 48 }}
   size={GoogleSigninButton.Size.Wide}
   color={GoogleSigninButton.Color.Dark}
   onPress={signIn}/>;*/}

            </ImageBackground>

          {/*  <View
                style={styles.separator}
                lightColor="#eee"
                darkColor="rgba(255,255,255,0.1)"
          >

            </View>*/}
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    title:{
        fontSize:20,
        fontWeight:"bold",
        alignItems: "center",
        alignContent:"center",
        textAlign:"center",
       marginTop: 60,
       color:"#fff"

    },
    separator: {
        marginVertical:30,
        height:1,
        width:"80%"
    },
    image:{
        marginTop:60,
        marginBottom: 20,
        width:100,
        height:100,
    },
    loginBtn:
 {
   width:"80%",
   borderRadius:12,
   height:40,
   alignItems:"center",
   justifyContent:"center",
   marginTop:10,
   backgroundColor:"#FF1493",
 },
 inactive_btn:{
    width:"80%",
    borderRadius:12,
    height:40,
    alignItems:"center",
    justifyContent:"center",
   /* backgroundColor:"#FB5BAA",*/
    marginTop:20,
     borderColor:"#FF1493",
     borderWidth:1,

 },
 inactive_btn_driver:{
   width:"82%",
   borderRadius:12,
   height:40,
   alignItems:"center",
   justifyContent:"center",
   marginTop:20,
   borderColor:"#FFCC93",
   borderWidth:2,
 }
 ,
 btnText: {
     color:"#fff"
 },
 btnTextDriver: { color:"yellow"}
 ,content_bg: {
     alignItems:"center"
 }
});
