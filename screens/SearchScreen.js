import { useNavigation } from "@react-navigation/native";
import { ImageBackground, Image, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { View, Text } from "../components/Themed";
import { SearchBar,Card } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import React, {useState, useEffect} from 'react';
import List from "../components/ListResults";
import Product from "../components/ProductResults";
import HeaderApp from "../components/HeaderApp";

export default function SearchScreen({route}) {
    
    const navigation = useNavigation();
    const [code, setCode] = React.useState(route.params.code);

    console.log(route.params.code);
    return(
        <View style={styles.container}>
    <HeaderApp name="Resultados" />
             <ScrollView style={styles.scroll}>
             <ImageBackground  source={require('../assets/images/bg-menu.png')}
            style={[styles.content_bg,{width: '100%', height: '100%'}]}>
          <View style={{backgroundColor:"#ff00ff00" , top: 0, left: 0, right: 0, bottom: 0,marginLeft:15, justifyContent: 'center', alignItems: "flex-start"}}>
   </View> 
     <View style={{backgroundColor:"#ff00ff00" , top: 15,marginLeft:15, justifyContent: 'center', alignItems: "flex-start"}}>
     <Text style={{color:"#fff", fontWeight:"bold"}}>Se encontraron resultados para: {code}</Text>
     </View> 
     <View style={styles.productContainer}>
         <Product code={code}/>
     </View>   
  
</ImageBackground>
</ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({

    container: { 
        flex:1,
        alignItems:"center",
        justifyContent:"center",
    },
    scroll:{
      width:"100%"
    },
    card:{
        marginTop:10,
        flexDirection:"row",
        width:"100%",
        borderRadius:15,
    },
    title:{
        color:"#000",
        fontWeight:"bold",
        marginTop:20,
        fontSize:16
    },
    details:{
        color:"#c7c7c7",
        fontWeight:"bold" ,
        marginTop:0,
        fontSize:15
    },
    price:{
        color:"#888",
        fontWeight:"bold",
        marginTop:5,
    },
    qualify:{
        color:"orange",
        fontWeight:"bold",
        marginTop:10,
    },
    textInputBorder:{
        marginLeft:15,
        marginTop:15,
        color:"#06623B",
        borderBottomColor:"pink",
        borderBottomWidth:1,
        width:"90%",
        fontSize:12,
        paddingLeft:10,
    },
    groupForm: {
        width:"90%",
        backgroundColor:"#ff00ff00",
        alignItems:"center"
    }
    ,
    image:{
        marginTop:10,
        marginBottom: 20,
        marginLeft: 15,
        width:400,
        height:100,
    },
    imageContainer:{
        flex:1,
        flexDirection:"row",
        backgroundColor:"#ff00ff00",
        marginTop:10,
        marginLeft:12,
    },
    productContainer:{
        flexDirection:"column",
        backgroundColor:"#ff00ff00",
        marginTop:20,
        width:"100%",
        height:"100%"
    },
    productRow:{
        padding:10,
        backgroundColor:"#ff00ff00",
        flexDirection:"column"      
    }
    ,
    imageCat:{
    margin:15,
        width:120,
        height:120
    },
    textCat:{
        fontSize:12,
        marginLeft:10,
        fontWeight:"300",
        color:"#F7067D"
    },
    imageOrder:{
        marginLeft:19,
        marginBottom:10,
            width:95,
            height:95
        },
    imageProd:{
        margin:10,
            width:60,
            height:60,
            borderRadius:10,
        }
    
})