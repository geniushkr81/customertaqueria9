import { useNavigation } from '@react-navigation/native';
import React, {useEffect,useState, Component } from 'react';
import { Image, StyleSheet,TextInput,ImageBackground, TouchableOpacity, Dimensions } from 'react-native';
import { View,Title,Caption, Text } from "../components/Themed";
import { Avatar } from 'react-native-elements';
import HeaderApp from '../components/HeaderApp';
import * as Location from 'expo-location';
import MapView, {Polyline, Marker} from 'react-native-maps';

export default function Profile (props) {
    const navigation = useNavigation();

    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
   // const [latitud, setLatitude] = useState(37.78825);
    //const [longitud, setLongitude] = useState(-122.4324);

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestBackgroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            setLocation(location);
        //    setLatitude(location.coords.latitude);
          //  setLongitude(location.coords.longitude);
        })();
    },[]);

    let text = 'Waiting...';
    if(errorMsg) {
        text = errorMsg;
    } else if (location) {
        text = JSON.stringify(location);
        console.log(location.coords.latitude);
    }
    
    return (
        <View style={styles.container}>
            <HeaderApp name="Perfil" />
            <View style={styles.header}></View>
            <Image style={styles.avatar} source={require('../assets/images/avatar.png')}/>
            <View style={styles.groupForm} >
        <TextInput style={styles.textInputBorder} placeholder="Nombre"></TextInput>
        <TextInput style={styles.textInputBorder} placeholder="Telefono"></TextInput>
        <TextInput style={styles.textInputBorder} placeholder="Email"></TextInput>
    </View>
    <View>
        <MapView style={styles.map}
             initialRegion={{
                 latitude:location.coords.latitude,
                 longitude:location.coords.longitude,
                 latitudeDelta: 0.0922,
                 longitudeDelta: 0.0421,
             }}
        >
            <Marker 
            coordinate={{
                latitude:location.coords.latitude,
                longitude:location.coords.longitude,
            }}
            title="Mi Docmicilio" />
        </MapView>
    </View>
        </View>
        
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    header:{
        backgroundColor: "#FEB72B",
        height:30
    },
    avatar:{
        width: 90,
        height: 90,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom:10,
        alignSelf:'center',
        position: 'absolute',
        marginTop:60
    },map:{
        alignSelf:"center",
        marginTop:30,
        width:350,
        height:200

    },
    textInputBorder:{
        marginLeft:15,
        marginTop:15,
        color:"#08823B",
        borderBottomColor:"#F7067D",
        borderBottomWidth:1,
        width:"90%",
        fontSize:12,
        paddingLeft:10,
    },
    groupForm: {
        width:"90%",
        backgroundColor:"#ff00ff00",
        alignItems:"center",
        marginTop:50
    }
})