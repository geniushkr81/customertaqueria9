import { useNavigation } from "@react-navigation/native";
import { ImageBackground,Image,TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { View,Text } from "../../components/Themed";

export default function LoginScreen(){
  const navigation = useNavigation();
    return(
        <View style={styles.container}>
         {/*   <ImageBackground  source={require('../assets/images/bg-sign.png')} 
            style={[styles.content_bg,{width: '100%', height: '100%'}]}> */}
      <Image style={styles.image} source={require("../../assets/images/logo.png")} />
      <View>
          <Text style={styles.title}>REPARTIDOR</Text>
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Usuario"
          placeholderTextColor="#003f5c"
        />
      </View>
 
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contrasena"
          placeholderTextColor="#003f5c"
          secureTextEntry={true}

        />
      </View>
 
      <TouchableOpacity>
        <Text style={styles.forgot_button}>Olvidaste tu contrasena ?</Text>
      </TouchableOpacity>
 
      <TouchableOpacity style={styles.loginBtn} onPress={()=>navigation.navigate('DeliverPanel')}>
        <Text style={styles.btnText}>Ingresar</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.loginBtn} 
      onPress={()=>navigation.navigate('Login')}>
        <Text style={styles.btnText}>Regresar</Text>
      </TouchableOpacity>
          {/*    </ImageBackground>
           
          <View
                style={styles.separator}
                lightColor="#eee"
                darkColor="rgba(255,255,255,0.1)"
          >

            </View>*/}
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    title:{
        fontSize:26,
        fontWeight:"bold",
        alignItems: "center",
        alignContent:"center",
        textAlign:"center",
       marginBottom:20,
       color:"orange"
        
    },
    separator: {
        marginVertical:30,
        height:1,
        width:"80%"
    },
    image:{
        marginTop:70,
        marginBottom: 40,
        width:100,
        height:100,
    },
    inputView: {
      backgroundColor: "#ff00ff00",
      borderRadius: 10,
      width: "80%",
      height: 35,
      marginBottom: 20,
      borderWidth:1,
      borderColor:"#06623B",
      alignItems:"flex-start",
     
    },
   
    TextInput: {
     color:"#06623B",
      height: 50,
      width: "80%",
      flex: 1,
      padding: 5,
      marginLeft: 10,
      fontWeight:"bold"
    },
   
    forgot_button: {
      height: 30,
      marginBottom: 30,
      color:"#06623B",
      fontWeight:"bold"
    },
    loginBtn:
 {
   width:"70%",
   borderRadius:10,
   height:40,
   alignItems:"center",
   justifyContent:"center",
   marginTop:10,
   backgroundColor:"orange",
  
 },
 inactive_btn:{
    width:"80%",
    borderRadius:12,
    height:40,
    alignItems:"center",
    justifyContent:"center",
    marginTop:10,
     borderColor:"#FF1493",
     borderWidth:1,
    
 },
 btnText: {
     color:"#fff",
     fontSize:15,
   fontWeight:"bold"
 }
 ,content_bg: {
     alignItems:"center"
 }
});