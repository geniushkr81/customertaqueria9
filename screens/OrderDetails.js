import { useNavigation } from "@react-navigation/native";
import { ImageBackground, Image, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { View, Text } from "../components/Themed";
import { SearchBar,Card, CheckBox } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import List from "../components/ListResults";
import Product from "../components/ProductResults";
import HeaderApp from "../components/HeaderApp";
import { Button } from "react-native-elements/dist/buttons/Button";
import { useState } from "react";
import { color } from "react-native-reanimated";

export default function OrderDetails() {
    const navigation = useNavigation();
    const [check1, setCheck1] = useState(false);
    const [check2, setCheck2] = useState(false);
    const [check3, setCheck3] = useState(false);
    const [check4, setCheck4] = useState(false);
    const [check5, setCheck5] = useState(false);
    const [check6, setCheck6] = useState(false);
    const [check7, setCheck7] = useState(false);
    const [check8, setCheck8] = useState(false);
    return (
        <View style={styles.container}>
            <HeaderApp name="Orden"/>
            <ScrollView style={styles.scroll}>
            {/* <View style={styles.row}>
                <Text style={styles.textAdd}>
                    Añadir algo más
                </Text>
            </View> */}
            <View style={styles.row}>
                <View style={styles.column}>
                <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
                </View>
                <View style={styles.column}>
                    <Text>Tacos al pastor</Text>
                </View>
                <View style={styles.column}>
                    <View style={styles.rowCircle}>
                        <View style={styles.column}>
                        <TouchableOpacity>
                        <Image style={styles.imageCircle} source={require("../assets/images/remove_circle.png")} />
                        </TouchableOpacity>
                        </View>
                        <View style={styles.column}>
                            <Text>1</Text>
                        </View>
                        <View style={styles.column}>
                            <TouchableOpacity>
                        <Image style={styles.imageCircle} source={require("../assets/images/add_circle.png")} />
                        </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
            {/* <View style={[styles.row,{marginTop:10}]}>
                <View style={[styles.column,{width:"70%"}]}>
                    <View style={styles.row}>
                        <Text>Tamaño</Text>
                    </View>
                </View>
                <View style={styles.column}>
                    <View style={styles.row}>
                        <Text>Precio</Text>
                    </View>
                </View>
            </View> */}
            {/* ITEMS SELECTION */}
            <View style={{marginTop:10}} ><Text style={styles.rowTitle}>Elementos</Text></View>
            <View style={styles.rowItems}>
            <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="lleva cilantro"
                checked={check1}
                onPress={() => setCheck1(!check1)}
            /></View>
             <View style={styles.rowItems}>
            <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="lleva cebolla"
                checked={check2}
                onPress={() => setCheck2(!check2)}
            />
            </View>
              <View style={styles.rowItems}>
             <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="lleva piña"
                checked={check3}
                onPress={() => setCheck3(!check3)}
            />
            </View>
            <View ><Text style={styles.rowTitle}>Salsas</Text></View>
            <View style={styles.row}>
                <View style={styles.column}>
                <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="Roja"
                checked={check4}
                onPress={() => setCheck4(!check4)}
            />
                </View>
                <View style={styles.column}>
                <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="Verde"
                checked={check5}
                onPress={() => setCheck5(!check5)}
            />
                </View>
                <View style={styles.column}>
                <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="Habanero"
                checked={check6}
                onPress={() => setCheck6(!check6)}
            />
                </View>
            </View>
            {/* Options Section */}
            <View ><Text style={styles.rowTitle}>Opciones</Text></View>
            <View style={styles.rowItems}>
            <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="Agua"
                checked={check7}
                onPress={() => setCheck7(!check7)}
            /></View>
             <View style={styles.rowItems}>
            <CheckBox
            containerStyle={styles.checkItems}
                center
                checkedColor="#F7067D"
                textStyle={{color:"#06623B"}}
                size={14}
                title="Postre"
                checked={check8}
                onPress={() => setCheck8(!check8)}
            />
            </View>
            {/* Options section */}
            <View ><Text style={styles.rowTitle}>Notas</Text></View>
            <View>
                <TextInput multiline={true} height={100} backgroundColor={"#FFE5BF"} />
            </View>
            <View style={styles.rowButtons}>
                <View style={styles.columnButtons}>
                    <TouchableOpacity style={styles.btnAdd} onPress={()=>navigation.navigate("Search")}>
                       <Text style={styles.btnTextAdd}>Añadir algo mas</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.columnButtons}>
                <TouchableOpacity style={styles.btnAdd} onPress={()=>navigation.navigate("OrderConfirm")}>
                       <Text style={styles.btnTextAdd}>Confirmar orden</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
        </View>
    )
}            

const styles = StyleSheet.create({
    container:{
        flex:1,
        
    },
    checkItems:{
        width:"100%",
        alignItems:"flex-start",
        backgroundColor:"#fff",
        borderWidth:0,
        padding:0
    }
    ,
    scroll:{
        width:"95%",
        padding:10,
        alignSelf:"center"
    },
    column:{
        flexDirection:"column"
    },
    row:{
        flexDirection:"row"
    },
    rowItems:{
        flexDirection:"row"
    },
    rowTitle:{
        width:"100%",
        backgroundColor:"#eeeeee",
        height:22,
        color:"#F7067D",
        fontSize:14,
        fontWeight:"bold",
        paddingLeft:10,
    },
    rowCircle:{
        flexDirection:"row",
        top:50,
        marginLeft:70
    },
    textAdd:{
        color:"#F7067D",
        fontWeight:"bold"
    }, 
    imageCat:{
        margin:5,
            width:70,
            height:70
    },
    rowButtons:{
        flexDirection:"row",
        width:"90%",
        marginBottom:30,
    },
    columnButtons:{
        flexDirection:"column",
        width:"55%",
    },
    btnAdd:{
        alignSelf:"center",
        borderRadius:8,
        height:30,
        width:"90%",
        color:"#fff",
        backgroundColor:"#ff00ff00",
        borderWidth:1,
        borderColor:"#f7067d",
        marginTop:10
    },
    btnTextAdd:{
        color:"#f7067d",
        textAlign:"center",
        padding:3,
        fontWeight:"bold"
    }
})