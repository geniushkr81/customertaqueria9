import { useNavigation } from "@react-navigation/native";
import { ImageBackground, Image, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { View, Text } from "../components/Themed";
import { SearchBar,Card, CheckBox } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import List from "../components/ListResults";
import Product from "../components/ProductResults";
import HeaderApp from "../components/HeaderApp";
import { Button } from "react-native-elements/dist/buttons/Button";
import { useState } from "react";
import { color } from "react-native-reanimated";


export default function OrderConfirm(){
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <HeaderApp name="Confirmar Orden" />
            <ScrollView style={styles.scroll}>
            {/* section resume order */}
            <View style={styles.resume}>
                <View style={styles.row}>
                    <View style={styles.column55}><Text style={styles.linkText}>Entregar</Text></View>
                    <View style={styles.column}><TouchableOpacity><Text style={styles.linkText}>Agregar nueva direccion</Text></TouchableOpacity></View>
                </View>
                <View style={styles.row}>
                    <View style={styles.column}>
                    <Image style={styles.imageMap} source={require("../assets/images/map.png")} />
                    </View>
                </View>
                <View style={styles.row}>
                <View style={styles.column}>
                <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
                </View>
                <View style={styles.column}>
                    <Text>Tacos al pastor</Text>
                </View>
                </View>
                <View style={[styles.row10]}>
                    <View style={styles.column65}><Text style={{color:"#F7067D", fontSize:16, fontWeight:"bold"}}>SubTotal</Text></View>
                    <View style={styles.column30}><Text style={{color:"#F7067D", fontSize:16, fontWeight:"bold"}}>$100</Text></View>
                </View>
                <View style={[styles.row10]}>
                    <View style={styles.column65}><Text style={{color:"#F7067D", fontSize:16, fontWeight:"bold"}}>Envio</Text></View>
                    <View style={styles.column30}><Text style={{color:"#F7067D", fontSize:16, fontWeight:"bold"}}>$20</Text></View>
                </View>
                <View style={[styles.rowTotal]}>
                    <View style={styles.column65}><Text style={{color:"#000", fontSize:18, fontWeight:"bold"}}>Total</Text></View>
                    <View style={styles.column30}><Text style={{color:"#000", fontSize:18, fontWeight:"bold"}}>$120</Text></View>
                </View>
                {/* related products */}
                <View style={[styles.rowProd]}>
                    <Image style={styles.image} source={require("../assets/images/cardprod.png")} />
                    <Image style={styles.image} source={require("../assets/images/cardprod.png")} />
                    <Image style={styles.image} source={require("../assets/images/cardprod.png")} />
                </View>
                {/* Payment method */}
                <View style={{marginTop:10}} ><Text style={styles.rowTitle}>Metodo de pago</Text></View>
                <View style={styles.row}>
                    <View style={styles.column}>
                    <Image  source={require("../assets/images/card01.png")} />
                    </View>
                    <View style={styles.column}>
                    <Image source={require("../assets/images/card02.png")} />
                    </View>
                </View>
                <View style={styles.row}>
                <TouchableOpacity style={styles.btnAdd} onPress={()=>navigation.navigate("TrackOrder")}>
                       <Text style={styles.btnTextAdd}>Confirmar</Text>
                    </TouchableOpacity>
                </View>

            </View>
            </ScrollView>
        </View>
    )

}

const styles =StyleSheet.create({
    container:{
        flex:1
    },
    textAdd:{
        color:"#F7067D",
        fontWeight:"bold"
    }, 
    scroll: {
        width:"98%",
        padding:10,
        alignSelf:"center"
    },
    rowTitle:{
        width:"100%",
        backgroundColor:"#eeeeee",
        height:22,
        color:"#F7067D",
        fontSize:14,
        fontWeight:"bold",
        paddingLeft:10,
    },
    resume:{
        flexDirection:"column",
        width:"98%"
    },
    rowProd:{
        alignSelf:"center",
        width:"97%",
        padding:10,
        flexDirection:"row",
        backgroundColor:"#eeeeee"
    },
    row:{
        alignSelf:"center",
        width:"97%",
        padding:10,
        flexDirection:"row"
    },
    row10:{
        width:"97%",
        padding:0,
        marginLeft:10,
        flexDirection:"row"
    },
    rowTotal:{
        width:"97%",
        marginTop:5,
        marginLeft:10,
        flexDirection:"row"
    },
    column:{
        width:"45%",
        flexDirection:"column"
    },
    column55:{
        width:"55%",
        marginLeft:0
    },
    column65:{
        width:"65%",
        marginLeft:10
    },
    column30:{
        width:"30%",
        marginLeft:40
    },
    linkText:{
        color:"#F7067D",
        fontSize:12,
        fontWeight:"bold"
    },
    imageMap:{
        width:330,
        height:130
    },
    image:{
        margin:5,
    },
    btnAdd:{
        alignSelf:"center",
        borderRadius:8,
        height:30,
        width:"100%",
        color:"#fff",
        backgroundColor:"#ff00ff00",
        borderWidth:1,
        borderColor:"#f7067d",
        marginTop:10,
        marginBottom:20
    },
    btnTextAdd:{
        color:"#f7067d",
        textAlign:"center",
        padding:3,
        fontWeight:"bold"
    }

})