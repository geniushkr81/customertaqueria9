import { useNavigation } from "@react-navigation/native";
import { Alert,ImageBackground,Modal, Pressable, Image, TextInput, StyleSheet, TouchableOpacity, Platform } from "react-native";
import { View, Text } from "../components/Themed";
import { SearchBar,Card } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import React, {useState, useEffect} from 'react';
import  axios  from 'axios';
import { create } from 'apisauce';
import BannersList from "../components/BannersList";
//import ShopCartIcon from "../components/ShopCartIcon";
import { navigationRef } from '../navigation/RootNavigation';

export default function PanelScreen() {
    const navigation = useNavigation();
    const [keywords, setKeywords] = React.useState('');
    const [modalVisible, setModalVisible] = useState(false);
    const [categories, setCategories] = React.useState([]);

    React.useEffect(() => {
        setModalVisible(true);
        axios.get('http://18.217.43.19:3000/categories/all')
            .then((res) => {
                setCategories(res.data);
            });
    }, [])
    
    // Get Categories from APIsauce
    const clientAPI = create ({
        baseURL: 'http://18.217.43.19:3000/categories/'
    });
    const req = async() => {
      response = await clientAPI.get('all')
    //Alert.alert(response.data);
    console.log(response.data);
    console.log(JSON.stringify(response.data[0]));
    return(
        <Text>Hi!!!</Text>
    )
    }
    //req()
    return(
        <View style={styles.container}>
             {/* MODAL COMPONENT */}
             <Modal
            animationType='slide'
            transparent={true}
            visible={modalVisible}
            onRequestClose={
              () => {
                Alert.alert('Modal has been closed.');
                setModalVisible(!modalVisible);
              }}>
                <View style={styles.centeredView}>
                  <View style={styles.modalView}>
                    <Image style={styles.imageModal} source={require("../assets/images/prod01.png")} />
                    <Pressable style={[styles.button, styles.buttonClose]} onPress={() => setModalVisible(!modalVisible)}>
                        <Text style={styles.textStyle}>Cerrar</Text>
                        </Pressable>
                  </View>
                </View>
              </Modal>
              {/* MODAL COMPONENT */}
             <ScrollView style={styles.scroll}>
                 {/** shopcart header */} 
             <ImageBackground  source={require('../assets/images/bg-menu.png')}
            style={[styles.content_bg,{width: '100%', height: '100%'}]}>
                <SearchBar round onChangeText={(val) => {setKeywords(val)}}
                onSubmitEditing={()=> navigation.navigate('Search')}
                placeholder="buscar"
                //console.log(`User typed ${keywords}`)}
                value={keywords}
                        //searchIcon={{color:"white", size:25 , color:"#fff", left:10}}
                        
                        platform={Platform.OS}
                        inputContainerStyle={{backgroundColor:"white", width:"96%", borderRadius:12, alignSelf:"center"}}
                        containerStyle={{backgroundColor:"#F7067D",borderRadius:12,borderWidth:0,marginLeft:13, width:"95%",marginTop:40}} />
                 {/* Banners */}
           <BannersList/>
              {/* Banners */}
              <View style={{backgroundColor:"#ff00ff00" , top: 0, left: 0, right: 0, bottom: 0,marginLeft:15, justifyContent: 'center', alignItems: "flex-start"}}>
     <Text style={{color:"#fff", fontWeight:"bold"}}>Categorias</Text>
   </View> 
   {/* Categories section */}
            <View style={styles.imageContainer}>
                <ScrollView horizontal={true}>
            {
                   categories.map((category) => {
                       return (
     <TouchableOpacity key={category.createdAt} onPress={() => navigation.navigate("Search",{code: category.code})}>
        <Image style={styles.imageCat} source={{uri:`http://18.217.43.19/uploads/${category.image}`}} />
        <Text style={styles.textCat}>{category.name}</Text></TouchableOpacity>
                       )
  }  )

}
        </ScrollView>
         </View>

      {/*  <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
       <Text style={styles.textCat}>Todo</Text>      
       </TouchableOpacity>
       <TouchableOpacity onPress={()=>{navigation.navigate("Search")}}>
       <Image style={styles.imageCat} source={require("../assets/images/cat02.png")} />
       <Text style={styles.textCat}>Tacos</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat03.png")} />
       <Text style={styles.textCat}>Alambres</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat04.png")} />
       <Text style={styles.textCat}>Tortas</Text>      
       </TouchableOpacity>
       <TouchableOpacity>
       <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
       <Text style={styles.textCat}>Todo</Text>      
       </TouchableOpacity>
      */}
     {/* Categories section */}
     <View style={{backgroundColor:"#ff00ff00" , top: 15,marginLeft:15, justifyContent: 'center', alignItems: "flex-start"}}>
     <Text style={{color:"#fff", fontWeight:"bold"}}>Lo mas vendido</Text>
     </View> 
     <View style={styles.productContainer}>
         <View style={styles.productRow}>
     <Image style={styles.imageProd} source={require("../assets/images/prod01.png")} />
     <Image style={styles.imageProd} source={require("../assets/images/prod02.png")} />
 
     <Image style={styles.imageProd} source={require("../assets/images/prod03.png")} />
     <Image style={styles.imageProd} source={require("../assets/images/prod04.png")} />
     </View>
     
     </View>   

    <View style={{flexDirection:"row", backgroundColor:"#ff00ff00" , top: 0, left: 0, right: 0, bottom: 0,marginLeft:15, alignContent:"space-between" }}>
     <Text style={{flex:1,color:"#fff", fontWeight:"bold"}}>Productos de la orden</Text>
     <Text style={{flex:2,alignItems:"flex-end", color:"#fff", fontWeight:"bold"}}>Todo</Text>
    </View>
    <View style={styles.imageContainer}>
     <Image style={styles.imageOrder} source={require("../assets/images/cat01.png")} />
     <Image style={styles.imageOrder} source={require("../assets/images/cat02.png")} />
     <Image style={styles.imageOrder} source={require("../assets/images/cat03.png")} />
    </View>   
    <View style={{flexDirection:"row", backgroundColor:"#ff00ff00" , top: 0, left: 0, right: 0, bottom: 0,marginLeft:15, alignContent:"space-between" }}>
     <Text style={{flex:1,color:"#fff", fontWeight:"bold"}}>Promociones</Text>
     <Text style={{flex:2,alignItems:"flex-end", color:"#fff", fontWeight:"bold"}}>Todo</Text>
    </View>
    <View style={styles.imageContainer}>
     <Image style={styles.imageOrder} source={require("../assets/images/cat01.png")} />
     <Image style={styles.imageOrder} source={require("../assets/images/cat02.png")} />
     <Image style={styles.imageOrder} source={require("../assets/images/cat03.png")} />
    </View>   
</ImageBackground>
</ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: { 
        flex:1,
        alignItems:"center",
        justifyContent:"center",
    },
    scroll:{
      width:"100%"
    },
    image:{
        marginTop:10,
        marginBottom: 20,
        marginLeft: 15,
        width:400,
        height:100,
    },
    imageContainer:{
        flex:1,
        flexDirection:"row",
        backgroundColor:"#ff00ff00",
        marginTop:10,
        marginLeft:12,
    },
    productContainer:{
        flexDirection:"column",
        backgroundColor:"#ff00ff00",
        marginTop:5,
        width:"100%",
        height:370
    },
    productRow:{
        flex:1,
        flexWrap:"wrap",
        padding:10,
        backgroundColor:"#ff00ff00",
        flexDirection:"row"      
    }
  ,
    textCat:{
        fontSize:12,
        marginLeft:10,
        fontWeight:"bold",
        color:"white"
    },
    imageOrder:{
        marginLeft:19,
        marginBottom:10,
            width:95,
            height:95
        },
    imageProd:{
        margin:10,
            width:150,
            height:150,
            borderRadius:15,
        },
    imageModal: {
        margin:5,
        width:240,
        height:210,
        borderRadius:5,
    }
        ,
    modalView: {
        margin: 20,
        backgroundColor: '#000',
        borderRadius: 10,
        padding: 35,
        alignItems: 'center',
        shadowColor:'#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius:4,
        elevation: 5,
    },
     button: {
    borderRadius: 5,
    padding: 5,
    marginTop:15,
    elevation: 2,
    width:100,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#F7067D',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
        backgroundColor: '#ff00ff00'
    },
        imageContainer:{
            flex:1,
            flexDirection:"row",
            backgroundColor:"#ff00ff00",
            marginTop:10,
            marginLeft:12,
        },
        imageCat:{
            margin:5,
                width:70,
                height:70,
                borderRadius:5,
                padding: 0,
                borderWidth:4,
                borderColor:"#F7067D",
        }
})
