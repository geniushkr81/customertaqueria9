import { useNavigation } from "@react-navigation/native";
import { ImageBackground,Image,TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { View,Text } from "../components/Themed";

export default function SignUpScreen(){
  const navigation = useNavigation();
    return(
        <View style={styles.container}>
            <ImageBackground  source={require('../assets/images/bg-login.png')}
            style={[styles.content_bg,{width: '100%', height: '100%'}]}>
      <Image style={styles.image} source={require("../assets/images/logo.png")} />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Nombre Completo"
          placeholderTextColor="#003f5c"
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Direccion"
          placeholderTextColor="#003f5c"
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Telefono"
          placeholderTextColor="#003f5c"
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email"
          placeholderTextColor="#003f5c"
        />
      </View>
 
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contrasena"
          placeholderTextColor="#003f5c"
          secureTextEntry={true}

        />
      </View>
 
 
      <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.btnText}>Registrarme</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.loginBtn} 
      onPress={()=>navigation.navigate('Login')}>
        <Text style={styles.btnText}>Regresar</Text>
      </TouchableOpacity>
            </ImageBackground>
           
          {/*  <View
                style={styles.separator}
                lightColor="#eee"
                darkColor="rgba(255,255,255,0.1)"
          >

            </View>*/}
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    title:{
        fontSize:20,
        fontWeight:"bold",
        alignItems: "center",
        alignContent:"center",
        textAlign:"center",
       marginTop: 60,
       color:"#fff"
        
    },
    separator: {
        marginVertical:30,
        height:1,
        width:"80%"
    },
    image:{
        marginTop:60,
        marginBottom: 20,
        width:100,
        height:100,
    },
    inputView: {
      backgroundColor: "#FFC0CB",
      borderRadius: 10,
      width: "80%",
      height: 35,
      marginBottom: 10,
      borderWidth:1,
      borderColor:"#FF1493",
      alignItems:"flex-start",
    },
   
    TextInput: {
      height: 50,
      width: "80%",
      flex: 1,
      padding: 5,
      marginLeft: 10,
      fontWeight:"bold"
    },
   
    forgot_button: {
      height: 30,
      marginBottom: 30,
      color:"#fff"
    },
    loginBtn:
 {
   width:"70%",
   borderRadius:10,
   height:40,
   alignItems:"center",
   justifyContent:"center",
   marginTop:10,
   backgroundColor:"#FF1493",
  
 },
 inactive_btn:{
    width:"80%",
    borderRadius:12,
    height:40,
    alignItems:"center",
    justifyContent:"center",
    marginTop:10,
     borderColor:"#FF1493",
     borderWidth:1,
    
 },
 btnText: {
     color:"#fff",
   fontWeight:"bold"
 }
 ,content_bg: {
     alignItems:"center"
 }
});