// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDxc-zlRS7avcUXj8dQl80BsNKCU1Lr5zE",
  authDomain: "taqueria-47786.firebaseapp.com",
  projectId: "taqueria-47786",
  storageBucket: "taqueria-47786.appspot.com",
  messagingSenderId: "312291137372",
  appId: "1:312291137372:web:c7a9a0f3d7df3b32805399",
  measurementId: "G-4P75EXG8C9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
