// If you are not familiar with React Navigation, check out the "Fundamentals" guide:
// https://reactnavigation.org/docs/getting-started
import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {Button} from "react-native";
import { navigationRef } from '../navigation/RootNavigation';

import NotFoundScreen from "../screens/NotFoundScreen";
import LoginScreen from "../screens/LoginScreen";
import SignScreen from "../screens/SignScreen";
import SignUpScreen from "../screens/SignUpScreen";
import BottomTabNavigator from "./BottomTabNavigator";
import LinkingConfiguration from "./LinkingConfiguration";
import PanelScreen from "../screens/PanelScreen";
import SearchScreen from "../screens/SearchScreen";
import DetailsProduct from "../screens/DetailsProduct";
import ProfileScreen from "../screens/ProfileScreen";
import OrderDetails from "../screens/OrderDetails";
import OrderConfirm from "../screens/OrderConfirm";
import TrackOrder from "../screens/TrackOrder";
import DeliverTrack from "../screens/DevliverTrack";
/* DELIVER SECTION */
import DeliverLogin from "../screens/deliver/LoginScreen";
import PanelDeliverScreen from "../screens/deliver/PanelDeliverScreen";

export default function Navigation({ colorScheme }) {
  return (
    <NavigationContainer ref={navigationRef}
      linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={LoginScreen} options={{headerShown:false}} />
    <Stack.Screen name="Sign" component={SignScreen} options={{headerShown:false}}/>
    <Stack.Screen name="SignUp" component={SignUpScreen} options={{headerShown:false}}/>
    <Stack.Screen name="Panel" component={PanelScreen}  options={{headerShown:false}} />
    <Stack.Screen name="Search" component={SearchScreen} options={{
      title:'Resultados',
      headerShown:false,
      headerStyle:{
        backgroundColor:'#FEB72B',
      },
      headerTitleStyle:{
        fontSize:17,
        fontWeight: 'bold',
        color:'#fff'
      }
    }} />
    <Stack.Screen name="Details" component={DetailsProduct} options={{headerShown:false}}/>
    <Stack.Screen name="Profile" component={ProfileScreen} options={{headerShown:false}} />
    <Stack.Screen name="OrderDetails" component={OrderDetails} options={{headerShown:false}} />
    <Stack.Screen name="OrderConfirm" component={OrderConfirm} options={{headerShown:false}} />
    <Stack.Screen name="TrackOrder" component={TrackOrder} options={{headerShown:false}} />
    <Stack.Screen name="DeliverTrack" component={DeliverTrack} options={{headerShown:false}} />
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{headerShown:false}} />
      <Stack.Screen
        name="NotFound"
        component={NotFoundScreen}
        options={{ title: "Oops!" }}
      />
    <Stack.Screen name="DeliverLogin" component={DeliverLogin} options={{headerShown:false}} />
    <Stack.Screen name="DeliverPanel" component={PanelDeliverScreen} options={{headerShown:false}} />
    </Stack.Navigator>
  );
}
