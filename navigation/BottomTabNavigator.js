// Learn more about createBottomTabNavigator:
// https://reactnavigation.org/docs/bottom-tab-navigator
import Ionicons from "@expo/vector-icons/Ionicons";
import { BottomTabBar, createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { useColorScheme, Image } from "react-native";

import Colors from "../constants/Colors";
import TabOneScreen from "../screens/TabOneScreen";
import TabTwoScreen from "../screens/TabTwoScreen";
import PanelScreen from "../screens/PanelScreen";
import OrdersScreen from "../screens/OrdersScreen";
import ProfileScreen from "../screens/ProfileScreen";
import OrderDetails from "../screens/OrderDetails";

const BottomTab = createBottomTabNavigator();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabOne"
      
      screenOptions={{
        tabBarShowLabel:true,
        tabBarActiveTintColor: Colors[colorScheme].tint }
      }
    >
      <BottomTab.Screen
        name="Home"
        component={TabHomeNavigator}
        options={{
          headerShown:false ,
          tabBarIcon: ({ color}) => (
            <Ionicons style={[{color: "#F7067D"}]} size={26} name={'home-outline'}/>  
        /*    <Image
            focused={focused}
            source={require('../assets/images/home.png')}
            tintColor={tintColor} //there is no need for this
          />*/
          ),
        }}
      />
     
        <BottomTab.Screen
        name="Pedido"
        component={TabOrderNavigator}
        options={{
          headerShown:false,
          tabBarIcon:({color}) => (
            <Ionicons style={[{color: "#F7067D"},{fontWeight:"bold"}]} size={26} name={'basket-outline'}/>  
          ),
        }}
      />
          <BottomTab.Screen
        name="Cupones"
        component={TabProfileNavigator}
        options={{
          tabBarIcon:({color}) => (
            <Ionicons style={[{color: "#F7067D"},{fontWeight:"bold"}]} size={26} name={'pricetags-outline'}/>  
          ),
        }}
      /> 
       <BottomTab.Screen
        name="Sucursales"
        component={TabOrdersNavigator}
        options={{
          headerShown:false,
          tabBarIcon:({color}) => (
            <Ionicons style={[{color: "#F7067D"},{fontWeight:"bold"}]} size={26} name={'book-outline'}/>  
         /*   <Image 
            source={require('../assets/images/cart.png')}
            /> */
          ),
        }}
      />
        <BottomTab.Screen
        name="Perfil"
        component={TabProfileNavigator}
        options={{
          headerShown:false,
          tabBarIcon:({color}) => ( 
            <Ionicons style={[{color: "#F7067D"},{fontWeight:"bold"}]} size={27} name={'person-circle-outline'}/>  
          ),
        }}
      />
    
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props) {
  return <Ionicons size={22} style={{ marginBottom: 3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabHomeStack = createStackNavigator();

function TabHomeNavigator() {
  return (
    <TabHomeStack.Navigator>
      <TabHomeStack.Screen
        name="Panel"
        component={PanelScreen}
       options={{ headerShown:false }}
      />
    </TabHomeStack.Navigator>
  );
}

const TabOrdersStack = createStackNavigator();
function TabOrdersNavigator() {
  return (
    <TabOrdersStack.Navigator>
      <TabOrdersStack.Screen 
        name="Orders"
        component={OrdersScreen}
        options={{headerShown:false}}
      />
    </TabOrdersStack.Navigator>
  );
}

const TabOrderStack = createStackNavigator();
function TabOrderNavigator() {
  return(
    <TabOrderStack.Navigator>
    <TabOrderStack.Screen 
      name="Order"
      component={OrderDetails}
      options={{headerShown:false}}
    />
  </TabOrderStack.Navigator>
  );
}

const TabProfileStack = createStackNavigator();

function TabProfileNavigator(){
  return (
    <TabProfileStack.Navigator>
      <TabProfileStack.Screen 
        name="Profile"
        component={ProfileScreen}
        options={{headerShown:false}}
        />
    </TabProfileStack.Navigator>
  )
}

