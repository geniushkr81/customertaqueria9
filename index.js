import { registerRootComponent } from 'expo';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import store from "./stores";
import App from './App';
//
//
// ReactDOM.render(
// 	<Provider store={store}>
// 	   <App/>
// 	</Provider>,
// 	document.getElementById("root")
// );

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
