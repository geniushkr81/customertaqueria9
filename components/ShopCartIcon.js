import { Text, View, StyleSheet, Platform } from 'react-native';
import React, { Component } from 'react';
import Ionicons from "@expo/vector-icons/Ionicons";
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';


const ShopCartIcon = (props) => {
  
  <View style={[{padding:5}, Platform.OS == 'android' ? styles.iconContainer: null]}>
    <View style={{
      position: 'absolute'
    }}>
      <Text style={{color:'white', fontWeight: 'bold'}}>{props.cartItems.length}</Text>
    </View>
    <Ionicons onPress={() => props.navigation.navigate('Cart')} style={[{color: "#F7067D"},{fontWeight:"bold"}]} size={26} name={'cart'}/>  
  </View>
}

const mapStateToProps = (state) => {
  return {
    cartItems: state
  }
}

export default connect(mapStateToProps)
(withNavigation(ShopCartIcon));

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    paddingLeft: 20, paddingTop: 10, marginRight: 5
  }
});