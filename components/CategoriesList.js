import { useNavigation } from '@react-navigation/native';
import React, {useEffect,useState,Component} from 'react';
import axios from 'axios';
import { Image, StyleSheet, ImageBackground,View, Text, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

function CategoriesList() {
    const navigation = useNavigation();
    var [categories, setCategories] = useState([]);


  const getCategories = async () => {
        const response = await axios.get('http://18.217.43.19:3000/categories/all')
            .then(res => {
                const cats = res.data;
                setCategories(cats);
            })
        
           
        //const data = await response.data;
        //console.log(JSON.stringify(data));
        //setCategories(JSON.stringify(data));
  }
useEffect(() => {
   getCategories();
}, []);

 
}
export default CategoriesList