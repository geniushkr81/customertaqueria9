import { useNavigation } from '@react-navigation/native';
import React, { Component } from 'react';
import { Image, StyleSheet,ImageBackground, TouchableOpacity } from 'react-native';
import { View, Text } from "./Themed";
import axios from 'axios';


const Product = (props) => {
    const navigation = useNavigation();
    const [products, setProducts] = React.useState([]);

    React.useEffect(() => {
        url = `http://18.217.43.19:3000/products/${props.code}`;
        console.log(url);
        axios.get(url)
        .then((res) => {
            setProducts(res.data);
        });
    },[])
    
    return (
        
            products.map((product) => { 
            return (
        <TouchableOpacity style={styles.containerCard} onPress={()=>{
            navigation.navigate(props.url);
        }}>
        <View style={styles.card}>
        <View style={{flexDirection:"column", backgroundColor:"#ff00ff00"}}>
        <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} />
        </View>
        <View style={{flexDirection:"column", backgroundColor:"#ff00ff00"}}>
            <View style={{flexDirection:"row", backgroundColor:"#ff00ff00"}}><Text style={styles.title}>{props.code}</Text></View>
            <View style={{flexDirection:"row", backgroundColor:"#ff00ff00"}}><Text style={styles.details}>Orden 3 tacos de pastor</Text></View>
            <View style={{flexDirection:"row", backgroundColor:"#ff00ff00"}}><Text style={styles.price}>$10.0</Text></View>
            <View style={{flexDirection:"row", backgroundColor:"#ff00ff00"}}>
                <ImageBackground style={{width: 70, height: 30, top:10}} source={require("../assets/images/value.png")} >
                <Text style={styles.qualify}>4.5</Text>
                </ImageBackground>
                </View>
        </View>
    </View>
    </TouchableOpacity>
            )
        })
    ) 
}

const styles = StyleSheet.create({
    containerCard:{
        alignContent:"center",
        marginLeft:5
    },
card:{
    marginTop:10,
    flexDirection:"row",
    width:"98%",
    backgroundColor:"#ffffff99",
    borderRadius:15,
},
title:{
    color:"#000",
    fontWeight:"bold",
    marginTop:20,
    fontSize:16
},
details:{
    color:"#c7c7c7",
    fontWeight:"bold" ,
    marginTop:0,
    fontSize:15
},
price:{
    color:"#888",
    fontWeight:"bold",
    marginTop:5,
},
qualify:{
    color:"white",
    fontWeight:"bold",
    marginLeft:33,
    top:7
},
imageCat:{
    margin:15,
        width:120,
        height:120
    },
})

export default Product;