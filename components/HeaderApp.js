import React, { Component } from 'react';
import { useNavigation } from '@react-navigation/native';
import { Image, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';
import { View, Text } from "./Themed";
import { Ionicons } from '@expo/vector-icons';

const HeaderApp = (props) => {
    const navigation = useNavigation();
    return (
        <View style={styles.header}>
            <View style={styles.rowView}>
                <View style={styles.columnView}>
                    <TouchableOpacity onPress={()=> navigation.navigate('Root')}>
                    <Ionicons style={[{color: "#F7067D"},{fontWeight:"bold"},{top:15, left:-10}]} size={32} name={'arrow-back-circle'}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.columnView}>
                <Text style={styles.headerText}>{props.name}</Text>
                </View>
                <View style={styles.columnView}>
                    {/* <Ionicons 
                    onPress={()=>navigation.navigate("Profile")}
                    style={[{color: "#F7067D"},{fontWeight:"bold", top:10, right:-90}]} size={27} name={'person-circle-outline'}/> */}
                </View>
        </View></View>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor:"#FEB72B",
        width:"100%",
        alignItems:"center",
        padding:25,
    },
    headerText:{
        textAlign:"center",
        color:"white",
        top:10,
        fontWeight:"bold",
        fontSize:15,
    },
    rowView:{
        flexDirection:"row",
        backgroundColor:"#ff00ff00"
    },
    columnView:{
        flexDirection:"column",
        width:"33%",
        backgroundColor:"#ff00ff00",
    }
})

export default HeaderApp;