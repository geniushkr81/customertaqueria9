import { useNavigation } from '@react-navigation/native';
import React, {Component} from 'react';
import axios from 'axios';
import { Image, StyleSheet, ImageBackground,View, Text, TouchableOpacity } from 'react-native';
import { ScrollView } from "react-native-gesture-handler";


export default class BannersList extends React.Component {
    state = {
        banners: []
    }
    componentDidMount() {
        axios.get('http://18.217.43.19:3000/banners/all')
            .then(res => {
                const banners = res.data;
                this.setState({banners});
            })
    }
    render() {
        return (
            <View style={styles.imageContainer}>
                <ScrollView horizontal={true}>
            {
                    this.state.banners
                        .map(banners =>
     <TouchableOpacity key={banners.code}>
        <Image style={styles.imageBanner} source={{uri:`http://18.217.43.19/uploads/${banners.image}`}} />
       {/* <Image style={styles.imageCat} source={require("../assets/images/cat01.png")} /> */}
        </TouchableOpacity>
        )}
        </ScrollView>
         </View>
        )
    }
}
const styles = StyleSheet.create({
    imageContainer:{
        flex:1,
        flexDirection:"row",
        backgroundColor:"#ff00ff00",
        marginTop:10,
        marginLeft:12,
    },
    image:{
        marginTop:10,
        marginBottom: 20,
        marginLeft: 15,
        width:400,
        height:100,
    },
    imageBanner:{
        margin:2,
            width:340,
            height:90,
            borderRadius:5,
            padding: 0,
            borderWidth:4,
            borderColor:"#F7067D",
        },
        textCat:{
            fontSize:12,
            marginLeft:10,
            fontWeight:"bold",
            color:"white"
        },
})
