import React from 'react';
import { Button } from 'react-native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';

async function onGoogleButtonPress() {
  const { idToken } = await GoogleSignin.signIn();
  // create google credentials with the token
  const googleCredential = auth.GoogleAuthProvider.credential(idToken);
  // sign in the user with the credentials
  return auth().signInWithCredential(googleCredential);
}

function GoogleSignIn() {
  return (
    <Button
      title= "Google Sign-In"
      onPress={() => onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}
      />
  );
}



export default GoogleSignIn
